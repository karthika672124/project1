clc;
clear all;
close all;
 N=10^4;                             
 data_user1= rand(1,N)>0.5;          % Generation of data for user1
data_user1bpsk = 2*data_user1-1;    
n =4;                               %Number of  Data Sub-Carriers
walsh=hadamard(n);              
code1=walsh(2,:);                   
data_user11=data_user1bpsk';
tx=data_user11*code1; 
SNR = [1:20];
%SNR=10.^(SNRDB/10);
for k=1:length(SNR);
   
y=tx + awgn(tx,SNR(k));



recdata11=(code1*y')';
recdata12=real(recdata11)>0;
errors_user1(k) = size(find([data_user1 - recdata12]),2); %Errors for User1
SBer1 = errors_user1/N;                              

 
end

figure(1)
semilogy(SNRDB,SBer1,'r','linewidth',2),grid on,hold on;
